Rancher catalog for media servers


Steps:


Rancher
Add label on host for traefik --> Infrastructure --> hosts --> edit --> label --> traefik_lb = true



Mailu
To use the Let's Encrypt stack to generate the certificates and use them inside Mailu, you can do the following:

Note: for this example let's assume that your domain is example.com and that the Mailu ${ROOT} directory is at /mailu/. Update it as according to your configuration.

Create the Let's Encrypt stack from the Rancher Catalog following its documentation.
Enter in the Let's Encrypt stack and click the "upgrade" button of the letsencrypt service.
Go to the "Volumes" tab, it will have a Docker named volume as lets-encrypt:/etc/letsencrypt.
Update that named volume to be a host volume, for example: /etc/letsencrypt-example.com:/etc/letsencrypt.
Now, after creating the certificates (or renovating them) you will have your certificates as: fullchain.pem and privkey.pem.
Those certificates will be under the path: /etc/letsencrypt-example.com/production/certs/example.com/.
Now, you have your certificates in your host in that path, but you need them inside the Mailu path, i.e. in: /mailu/certs/cert.pem and /mailu/certs/key.pem.
To achieve that, create the /mailu/certs/ directory (if you haven't already):
mkdir -p /mailu/certs/
Go to that directory:
cd /mailu/certs/
And there, create a link (a hard link, not a symbolic link) to those files, but with the names that you need to have inside (key.pem and cert.pem):
ln /etc/letsencrypt-example.com/production/certs/example.com/privkey.pem key.pem
ln /etc/letsencrypt-example.com/production/certs/example.com/fullchain.pem cert.pem
Now you can start (or re-start) your Mailu Stack.

Finally, you must create the initial admin user account:

docker exec -it admin /bin/bash
python manage.py admin root example.net password

This will create a user named root@example.net with password password and administration privileges.

Bind
create ACL in Webmin and add it to /etc/bin/named.conf.options
recursion yes;
     allow-query { ACL; };
